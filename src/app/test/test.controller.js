export class TestController{
  constructor(CheckAuthService, $http,toastr,$stateParams, $localStorage, $state, envService){
    'ngInject';
    this.$http = $http;
    this.toastr = toastr;
    this.$localStorage = $localStorage;
    this.$state = $state;
    this.$stateParams = $stateParams;
    var vm = this;
    vm.logout = CheckAuthService.logout;
    vm.state = 'userTest';

    this.getTests = ()=>{
      let id = this.$stateParams.id;
      $http({
        url : envService.read('apiUrl')+"api/test",
        method : "GET"
      }).then(function successCallback(response) {
        console.log(response);

        vm.tests = response.data.tests;

      }, function errorCallback(response) {
        console.log('Ошибка запроса');
      });
    };


    this.getTests();
  }


}
