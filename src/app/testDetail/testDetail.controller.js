export class TestDetailController{
  constructor(CheckAuthService, $http,toastr,$stateParams, $localStorage, $state, envService){
    'ngInject';
    this.$http = $http;
    this.toastr = toastr;
    this.$localStorage = $localStorage;
    this.$state = $state;
    this.$stateParams = $stateParams;
    var vm = this;
    vm.logout = CheckAuthService.logout;
    vm.state = 'userTest';
    // theory, practical, table
    vm.typeTask = ['theory', 'practical', 'table', 'asFile'];
    vm.typeTask = vm.typeTask.map(function (item) {
      return {value: item, label: `<span class="m-selected-span">${item}</span>`}
    });
    vm.typeQuestion = ['table', 'text', 'test', 'file'];
    vm.typeQuestion = vm.typeQuestion.map(function (item) {
      return {value: item, label: `<span class="m-selected-span">${item}</span>`}
    });

    this.getTests = ()=>{
      let id = this.$stateParams.id;
      $http({
        url : `${envService.read('apiUrl')}api/test/${id}`,
        method : "GET"
      }).then(function successCallback(response) {
        console.log(response);

        vm.test = response.data.test;

      }, function errorCallback(response) {
        vm.test = {
          testA:[],
          testB:[]
        };
        console.log('Ошибка запроса');
      });
    };

    this.getUsers = ()=>{
      let id = this.$stateParams.id;

      $http({
        url : `${envService.read('apiUrl')}api/user`,
        method : "GET"
      }).then(function successCallback(response) {
        console.log(response);


        vm.users = response.data.users.map(function (item) {
          return {value: item._id, label: `<span class="m-selected-span">${item.name}</span>`}
        });;

      }, function errorCallback(response) {
        console.log('Ошибка запроса');
      });
    };



    this.saveTest = (test) =>{
      console.log(test);
      let id = this.$stateParams.id;
      $http({
        url : `${envService.read('apiUrl')}api/test/${id}`,
        method : "PUT",
        data : test
      }).then(function successCallback(response) {
        console.log(response);
        vm.tests.push(response.data.test);
        vm.newUser = {};
        vm.newUser.test = vm.typeTests[0].value;
        vm.toastr.success('Добавлено', `Запись добавлена, пиьсмо отправлено на почту ${response.data.test.email}`);

      }, function errorCallback(response) {
        console.log('Ошибка запроса');
      });
    };


    this.getTests();
    this.getUsers();

  }


}
