export class UserTestDetailController{
  constructor(CheckAuthService, $http,toastr, $localStorage, $state,
              envService, $stateParams, $location, $interval){
    'ngInject';
    this.$http = $http;
    this.toastr = toastr;
    this.envService = envService;
    this.$localStorage = $localStorage;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.$location = $location;
    var vm = this;
    vm.logout = CheckAuthService.logout;
    vm.selectedTest="theory";
    this.timer;
    this.startParam = this.$location.search().start;
    this.offset = new Date().getTimezoneOffset();
    this.theory;
    this.practical;

    this.getTest = ()=>{
      let userId = this.$localStorage.user._id;
      let id = this.$stateParams.id;

      $http({
        url : envService.read('apiUrl')+`api/userTest/${id}/?userId=${userId}&start=${vm.startParam}`,
        method : "GET"
      }).then(function successCallback(response){

        vm.test = response.data.test;
        console.log("Test",vm.test);
        if (vm.startParam) {
          if (vm.test.startDate) {
            let current = new Date();
            let time = new Date(vm.test.startDate);
            let difference = current - time;
            if (difference<=30*60*1000){
              vm.theory = true;
            }
            if(difference<=24*60*60*1000){
              vm.practical = true;
            }
            console.log(difference);
            console.log(vm.offset);
            vm.timeTheory = vm.offset * 60 * 1000;
            if (vm.theory) {
              console.log('asds');
              vm.timeTheory = 30 * 60 * 1000 - difference;
              vm.timeTheory = vm.timeTheory + (vm.offset * 60 * 1000);
            }
            vm.timePractical = vm.offset * 60 * 1000;
            if (vm.practical) {
              vm.timePractical = 24 * 60 * 60 * 1000 - difference;
              console.log(vm.timePractical);
              vm.timePractical = vm.timePractical + (vm.offset * 60 * 1000);
              console.log(vm.timeTheory);
            }
            if (!vm.theory){
              if (!vm.practical){
                vm.toastr.success('Внимание!', 'Время сдачи теста прошло, и изменить вы его е сможете!');
              }else {
                vm.selectedTest = "practical";
              }
            }

            vm.timer = $interval(vm.timerCheck, 1000);
          }
        }

      }, function errorCallback(response) {
        console.log('Ошибка запроса');
      });
    };

    this.setSumm = (task)=>{
      let userId = this.$localStorage.user._id;
      $http({
        url : envService.read('apiUrl')+`api/task/${task._id}`,
        method : "POST",
        data: { task, userId }
      }).then(function (response){
      console.log(response.data.message);
        vm.toastr.success('Внимание!', response.data.message);



      }, function (response) {
        vm.toastr.success('Ошибка!', 'Что то пошло не так.');
        console.log('Ошибка запроса');
      });
    };

    this.timerCheck = ()=>{
      console.log('timer check');
      if (vm.timeTheory!=vm.offset * 60 * 1000){
        vm.timeTheory = vm.timeTheory - 1000;
      }else{
        if (!vm.test.finishA)
          vm.closeTestA(vm.test);
      }
      if (vm.timePractical!=vm.offset * 60 * 1000){
        vm.timePractical = vm.timePractical - 1000;
      }else{
        if (!vm.test.finishB)
          vm.closeTestB(vm.test);
      }
    };

    this.closeTestA = (test)=>{
      let userId = this.$localStorage.user._id;
      let id = this.$stateParams.id;
      if (test.finishA){
        vm.toastr.error('Внимание!', 'Вы уже сдали теорию');
        vm.selectedTest = 'practical';
        return;
      }else{
        test.finishA = true;
      }

      $http({
        url : envService.read('apiUrl')+"api/userTest/"+id,
        method : "PUT",
        data : {
          test
        }
      }).then(function successCallback(response) {
        vm.toastr.success('Спасибо!', 'Ваши результаты по теории сохранены');
        vm.selectedTest = 'practical';

      }, function errorCallback(response) {
        console.log('Ошибка запроса');
      });
    };

    this.closeTestB = (test)=>{
      let userId = this.$localStorage.user._id;
      let id = this.$stateParams.id;
      if (test.finishB){
        vm.toastr.error('Внимание!', 'Вы уже сдали практику');

        return;
      }else{
        test.finishB = true;
      }

      $http({
        url : envService.read('apiUrl')+"api/userTest/"+id,
        method : "PUT",
        data : {
          test
        }
      }).then(function successCallback(response) {
        vm.toastr.success('Спасибо!', 'Ваши результаты по практике сохранены');



      }, function errorCallback(response) {
        console.log('Ошибка запроса');
      });
    };



    this.getTest();
  }


}
