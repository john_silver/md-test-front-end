export function routerConfig ($stateProvider, $urlRouterProvider) {
  'ngInject';
  $stateProvider
    .state('home', {
      templateUrl: 'app/main/main.html'
    })
    .state('auth', {
      url: '/',
      templateUrl: 'app/auth/auth.html',
      controller: 'AuthController',
      controllerAs: 'vm',
      data: {
        'noLogin': true
      }
    })
    .state('userTest', {
      url: '/userTest',
      templateUrl: 'app/userTest/userTest.html',
      controller: 'UserTestController',
      controllerAs: 'vm',
      parent : 'home'
    })
    .state('userTestDetail', {
      url: '/userTest/:id',
      templateUrl: 'app/userTestDetail/userTestDetail.html',
      controller: 'UserTestDetailController',
      controllerAs: 'vm',
      parent : 'home',
      data: {
        'noLogin': true
      }
    })
    .state('test', {
      url: '/test',
      templateUrl: 'app/test/test.html',
      controller: 'TestController',
      controllerAs: 'vm',
      parent : 'home'
    })
    .state('testDetail', {
      url: '/test/:id',
      templateUrl: 'app/testDetail/testDetail.html',
      controller: 'TestDetailController',
      controllerAs: 'vm',
      parent : 'home'
    });

  $urlRouterProvider.otherwise('/');
}
