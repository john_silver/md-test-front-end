/* global malarkey:false, moment:false */

import { config } from './index.config';
import { routerConfig } from './index.route';
import { runBlock } from './index.run';
import { MainController } from './main/main.controller';
import { AuthController } from './auth/auth.controller';
import { UserTestController } from './userTest/userTest.controller';
import { UserTestDetailController } from './userTestDetail/userTestDetail';
import { TestController } from './test/test.controller';
import { TestDetailController } from './testDetail/testDetail.controller';


import { NavbarDirective } from '../app/components/navbar/navbar.directive';
import { FooterDirective } from '../app/components/footer/footer.directive';
import { DynamicTextArea } from '../app/components/dynamicTextArea/dynamicTextArea.directive';

import { CheckAuthService } from '../app/components/checkAuth/checkAuth.service';

angular.module('ticketMirusDesk', ['ngAnimate', 'ngCookies', 'ngSanitize', 'ngMessages', 'ngAria', 'ngResource',
  'ui.router', 'ngMaterial', 'toastr', 'lfNgMdFileInput', 'ui.mask','mgcrea.ngStrap','monospaced.elastic',
  '720kb.datepicker', 'tmh.dynamicLocale', 'ngStorage', 'naif.base64', 'environment', 'base64'])
  .constant('malarkey', malarkey)
  .constant('moment', moment)
  .config(config)
  .config(routerConfig)

  .run(runBlock)
  .controller('MainController', MainController)
  .controller('AuthController', AuthController)
  .controller('UserTestController', UserTestController)
  .controller('UserTestDetailController', UserTestDetailController)
  .controller('TestController', TestController)
  .controller('TestDetailController', TestDetailController)

  .directive('acmeNavbar', NavbarDirective)
  .directive('mFooter', FooterDirective)
  .directive('dynamicTextArea', DynamicTextArea)

  .service('CheckAuthService', CheckAuthService);

