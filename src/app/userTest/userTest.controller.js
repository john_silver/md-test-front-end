export class UserTestController{
  constructor(CheckAuthService, $http,toastr,$stateParams, $localStorage, $state, envService){
    'ngInject';
    this.$http = $http;
    this.toastr = toastr;
    this.$localStorage = $localStorage;
    this.$state = $state;
    this.$stateParams = $stateParams;
    var vm = this;
    vm.logout = CheckAuthService.logout;
    vm.state = 'userTest';

    this.getTests = ()=>{
      let id = this.$stateParams.id;
      $http({
        url : envService.read('apiUrl')+"api/userTest",
        method : "GET"
      }).then(function successCallback(response) {
        console.log(response);

        vm.tests = response.data.tests;

      }, function errorCallback(response) {
        console.log('Ошибка запроса');
      });
    };

    this.typeTests = [];

    this.getTypeTests = ()=>{
      let id = this.$stateParams.id;
      $http({
        url : envService.read('apiUrl')+"api/test",
        method : "GET"
      }).then(function successCallback(response) {
        console.log(response);

        // vm.typeTests = response.data.tests;
        vm.typeTests = [];
        response.data.tests.forEach(function (item) {
          vm.typeTests.push({value: item._id, label: `<span class="m-selected-span">${item.name}</span>`});
        });
        vm.newUser = {};
        vm.newUser.test = vm.typeTests[0].value;

      }, function errorCallback(response) {
        console.log('Ошибка запроса');
      });
    };



    this.sendEmail = (tester) =>{
      console.log(tester);
      $http({
        url : envService.read('apiUrl')+"api/userTest/sendRating",
        method : "POST",
        data : tester
      }).then(function successCallback(response) {
        console.log(response);

        vm.toastr.success('Ура!', `Пиьсмо отправлено на почту ${response.data.test.email}`);

      }, function errorCallback(response) {
        console.log('Ошибка запроса');
      });
    };

    this.addTester = (tester) =>{
      console.log(tester);
      $http({
        url : envService.read('apiUrl')+"api/userTest",
        method : "POST",
        data : tester
      }).then(function successCallback(response) {
        console.log(response);
        vm.tests.push(response.data.test);
        vm.newUser = {};
        vm.newUser.test = vm.typeTests[0].value;
        vm.toastr.success('Добавлено', `Запись добавлена, пиьсмо отправлено на почту ${response.data.test.email}`);

      }, function errorCallback(response) {
        console.log('Ошибка запроса');
      });
    };


   this.getTests();
   this.getTypeTests();
  }


}
